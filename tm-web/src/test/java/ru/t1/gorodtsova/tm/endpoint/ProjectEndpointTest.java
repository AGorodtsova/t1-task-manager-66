package ru.t1.gorodtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.client.IProjectEndpointClient;
import ru.t1.gorodtsova.tm.marker.IntegrationCategory;
import ru.t1.gorodtsova.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private IProjectEndpointClient client = IProjectEndpointClient.client();

    @NotNull
    private Collection<Project> projects = new ArrayList<>();

    @NotNull
    private final Project project1 = new Project("Project 1");

    @NotNull
    private final Project project2 = new Project("Project 2");

    @NotNull
    private final Project project3 = new Project("Project 3");

    @Before
    public void initTest() {
        client.save(project1);
        client.save(project2);
        client.save(project3);
        projects = client.findAll();
    }

    @After
    public void clean() {
        client.delete(project1);
        client.delete(project2);
        client.delete(project3);
    }

    @Test
    public void save() {
        @NotNull final Project project4 = new Project("Project 4");
        client.save(project4);
        Project projectFind = client.findById(project4.getId());
        Assert.assertEquals(project4.getId(), projectFind.getId());
        client.delete(project4);
    }

    @Test
    public void findAll() {
        Collection<Project> projects1 = client.findAll();
        Assert.assertEquals(projects.size(), projects1.size());
    }

    @Test
    public void findById() {
        Project project = client.findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    public void delete() {
        @NotNull final String id = project2.getId();
        client.delete(project2);
        Assert.assertNull(client.findById(id));
    }

    @Test
    public void deleteById() {
        @NotNull final String id = project1.getId();
        client.deleteById(id);
        Assert.assertNull(client.findById(id));
    }

}
