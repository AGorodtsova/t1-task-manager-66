package ru.t1.gorodtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.client.ITaskEndpointClient;
import ru.t1.gorodtsova.tm.marker.IntegrationCategory;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.ArrayList;
import java.util.Collection;

@Category(IntegrationCategory.class)
public class TaskEndpointTest {

    @NotNull
    private ITaskEndpointClient client = ITaskEndpointClient.client();

    @NotNull
    private Collection<Task> tasks = new ArrayList<>();

    @NotNull
    private final Task task1 = new Task("Task 1");

    @NotNull
    private final Task task2 = new Task("Task 2");

    @NotNull
    private final Task task3 = new Task("Task 3");

    @Before
    public void initTest() {
        client.save(task1);
        client.save(task2);
        client.save(task3);
        tasks = client.findAll();
    }

    @After
    public void clean() {
        client.delete(task1);
        client.delete(task2);
        client.delete(task3);
    }

    @Test
    public void save() {
        @NotNull final Task task4 = new Task("Task 4");
        client.save(task4);
        Task taskFind = client.findById(task4.getId());
        Assert.assertEquals(task4.getId(), taskFind.getId());
        client.delete(task4);
    }

    @Test
    public void findAll() {
        Collection<Task> task1 = client.findAll();
        Assert.assertEquals(tasks.size(), task1.size());
    }

    @Test
    public void findById() {
        Task task = client.findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void delete() {
        @NotNull final String id = task2.getId();
        client.delete(task2);
        Assert.assertNull(client.findById(id));
    }

    @Test
    public void deleteById() {
        @NotNull final String id = task1.getId();
        client.deleteById(id);
        Assert.assertNull(client.findById(id));
    }

}
