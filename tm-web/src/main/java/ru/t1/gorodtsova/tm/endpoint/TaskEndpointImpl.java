package ru.t1.gorodtsova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.gorodtsova.tm.api.service.ITaskService;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.List;

@RestController
@RequestMapping("api/tasks")
public class TaskEndpointImpl {

    @Autowired
    private ITaskService taskService;

    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") String id) {
        return taskService.findById(id);
    }

    @PostMapping("/save")
    public void save(@RequestBody Task task) {
        taskService.save(task);
    }

    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") String id) {
        taskService.removeById(id);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody Task task) {
        taskService.remove(task);
    }

}
