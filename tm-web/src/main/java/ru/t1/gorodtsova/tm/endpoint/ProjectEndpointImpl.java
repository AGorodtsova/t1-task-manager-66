package ru.t1.gorodtsova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.gorodtsova.tm.api.service.IProjectService;
import ru.t1.gorodtsova.tm.model.Project;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @GetMapping("/findById/{id}")
    public Project findById(@PathVariable("id") String id) {
        return projectService.findById(id);
    }

    @PostMapping("/save")
    public void save(@RequestBody Project project) {
        projectService.save(project);
    }

    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") String id) {
        projectService.removeById(id);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody Project project) {
        projectService.remove(project);
    }

}
