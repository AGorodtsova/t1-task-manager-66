package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.api.repository.ITaskRepository;
import ru.t1.gorodtsova.tm.api.service.ITaskService;
import ru.t1.gorodtsova.tm.model.Task;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    public void create() {
        @NotNull final Task task = new Task("New task " + System.currentTimeMillis());
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void save(@Nullable final Task task) {
        if (task == null) throw new EntityNotFoundException();
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @Nullable
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void remove(@Nullable final Task task) {
        if (task == null) throw new EntityNotFoundException();
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        taskRepository.deleteById(id);
    }

}
