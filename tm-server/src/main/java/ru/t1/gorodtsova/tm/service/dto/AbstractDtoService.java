package ru.t1.gorodtsova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.api.repository.dto.IDtoRepository;
import ru.t1.gorodtsova.tm.api.service.dto.IDtoService;
import ru.t1.gorodtsova.tm.dto.model.AbstractModelDTO;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDtoService<M extends AbstractModelDTO> implements IDtoService<M> {

    @NotNull
    @Autowired
    private IDtoRepository<M> repository;

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.save(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@Nullable final Collection<M> models) {
        if (models == null) throw new EntityNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@Nullable final Collection<M> models) {
        if (models == null) throw new EntityNotFoundException();
        repository.deleteAll();
        repository.saveAll(models);
        return models;
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        repository.save(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Optional<M> model = repository.findById(id);
        if (!model.isPresent()) throw new EntityNotFoundException();
        return model.get();
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.deleteAll(collection);
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Optional<M> model;
        model = repository.findById(id);
        if (!model.isPresent()) throw new EntityNotFoundException();
        repository.deleteById(id);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public long getSize() {
        return repository.count();
    }

}
