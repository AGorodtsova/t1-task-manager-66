package ru.t1.gorodtsova.tm.api.repository.dto;

import ru.t1.gorodtsova.tm.dto.model.AbstractUserOwnedModelDTO;


public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends IDtoRepository<M> {
}
