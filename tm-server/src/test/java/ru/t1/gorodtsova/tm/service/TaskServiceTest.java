package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ru.t1.gorodtsova.tm.api.service.dto.IProjectDtoService;
import ru.t1.gorodtsova.tm.api.service.dto.ITaskDtoService;
import ru.t1.gorodtsova.tm.api.service.dto.IUserDtoService;
import ru.t1.gorodtsova.tm.configuration.ServerConfiguration;
import ru.t1.gorodtsova.tm.dto.model.TaskDTO;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.*;
import ru.t1.gorodtsova.tm.marker.UnitCategory;

import static ru.t1.gorodtsova.tm.constant.ProjectTestData.*;
import static ru.t1.gorodtsova.tm.constant.TaskTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private AbstractApplicationContext context;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private IProjectDtoService projectService;

    @NotNull
    private ITaskDtoService taskService;

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        userService = context.getBean(IUserDtoService.class);
        projectService = context.getBean(IProjectDtoService.class);
        taskService = context.getBean(ITaskDtoService.class);

        userService.add(USER_LIST);
        projectService.add(PROJECT_LIST);
        taskService.add(TASK_LIST);
    }

    @After
    public void tearDown() {
        taskService.removeAll();
        projectService.removeAll();
        userService.removeAll();
        context.close();
    }

    @Test
    public void add() {
        taskService.removeAll();
        taskService.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findAll().get(0).getId());
    }

    @Test
    public void addByUser() {
        taskService.removeAll();
        taskService.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findAll().get(0).getId());
        Assert.assertEquals(USER1.getId(), taskService.findAll().get(0).getUserId());
        thrown.expect(UserIdEmptyException.class);
        taskService.add(null, USER1_TASK2);
    }

    @Test
    public void set() {
        Assert.assertFalse(taskService.findAll().isEmpty());
        taskService.set(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), taskService.findAll().size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(TASK_LIST.size(), taskService.findAll().size());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(USER1_TASK_LIST.size(), taskService.findAll(USER1.getId()).size());
        Assert.assertNotEquals(USER1_TASK_LIST.size(), taskService.findAll(USER2.getId()).size());
        thrown.expect(UserIdEmptyException.class);
        taskService.findAll("");
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findOneById(USER1.getId(), USER1_TASK1.getId()).getId());

        thrown.expect(ModelNotFoundException.class);
        Assert.assertNull(taskService.findOneById(USER1.getId(), USER2_TASK1.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.findOneById(null, USER1_TASK1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        taskService.findOneById(USER1.getId(), null);
    }

    @Test
    public void removeAll() {
        Assert.assertFalse(taskService.findAll().isEmpty());
        taskService.removeAll();
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    public void removeAllByUserId() {
        Assert.assertFalse(taskService.findAll(USER1.getId()).isEmpty());
        taskService.removeAll(USER1.getId());
        Assert.assertTrue(taskService.findAll(USER1.getId()).isEmpty());
        thrown.expect(UserIdEmptyException.class);
        taskService.removeAll("");
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertTrue(taskService.existsById(USER1_TASK2.getId()));
        taskService.removeOneById(USER1.getId(), USER1_TASK2.getId());
        Assert.assertFalse(taskService.existsById(USER1_TASK2.getId()));

        thrown.expect(ModelNotFoundException.class);
        taskService.removeOneById(USER2.getId(), USER1_TASK1.getId());

        thrown.expect(UserIdEmptyException.class);
        taskService.removeOneById(null, USER1_TASK1.getId());

        thrown.expect(IdEmptyException.class);
        taskService.removeOneById(USER1.getId(), null);
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(taskService.existsById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(taskService.existsById(USER2.getId(), USER1_TASK1.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.existsById(null, USER1_TASK1.getId());

        thrown.expect(IdEmptyException.class);
        taskService.existsById(USER1.getId(), null);
    }

    @Test
    public void createTaskName() {
        @NotNull final TaskDTO task = taskService.create(USER1.getId(), "test task");
        Assert.assertEquals(task.getId(), taskService.findOneById(task.getId()).getId());
        Assert.assertEquals("test task", taskService.findOneById(task.getId()).getName());
        Assert.assertEquals(USER1.getId(), taskService.findOneById(task.getId()).getUserId());

        thrown.expect(UserIdEmptyException.class);
        taskService.create(null, USER1_TASK1.getName());

        thrown.expect(NameEmptyException.class);
        taskService.create(USER1.getId(), null);
    }

    @Test
    public void createTaskNameDescription() {
        @NotNull final TaskDTO task = taskService.create(USER1.getId(), "test task", "test description");
        Assert.assertEquals(task.getId(), taskService.findOneById(task.getId()).getId());
        Assert.assertEquals("test task", taskService.findOneById(task.getId()).getName());
        Assert.assertEquals("test description", taskService.findOneById(task.getId()).getDescription());
        Assert.assertEquals(USER1.getId(), taskService.findOneById(task.getId()).getUserId());

        thrown.expect(UserIdEmptyException.class);
        taskService.create(null, USER1_TASK1.getName(), USER1_TASK1.getDescription());

        thrown.expect(NameEmptyException.class);
        taskService.create(USER1.getId(), null, USER1_TASK1.getDescription());

        thrown.expect(DescriptionEmptyException.class);
        taskService.create(USER1.getId(), "test task", null);
    }

    @Test
    public void updateById() {
        taskService.updateById(USER1.getId(), USER1_TASK2.getId(), "new name", "new description");
        Assert.assertEquals("new name", taskService.findOneById(USER1_TASK2.getId()).getName());
        Assert.assertEquals("new description", taskService.findOneById(USER1_TASK2.getId()).getDescription());

        thrown.expect(UserIdEmptyException.class);
        taskService.updateById(null, USER1_TASK2.getId(), "new name", "new description");

        thrown.expect(IdEmptyException.class);
        taskService.updateById(USER1.getId(), null, "new name", "new description");

        thrown.expect(NameEmptyException.class);
        taskService.updateById(USER1.getId(), USER1_TASK2.getId(), null, "new description");

        thrown.expect(ProjectNotFoundException.class);
        taskService.updateById(USER2.getId(), USER1_TASK2.getId(), "new name", "new description");
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertEquals(Status.NOT_STARTED, taskService.findOneById(USER1_TASK1.getId()).getStatus());
        taskService.changeTaskStatusById(USER1.getId(), USER1_TASK1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findOneById(USER1_TASK1.getId()).getStatus());

        thrown.expect(UserIdEmptyException.class);
        taskService.changeTaskStatusById(null, USER1_TASK1.getId(), Status.IN_PROGRESS);

        thrown.expect(ProjectNotFoundException.class);
        taskService.changeTaskStatusById(USER2.getId(), USER1_TASK1.getId(), Status.IN_PROGRESS);
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertEquals(USER1_TASK_LIST.size(), taskService.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()).size());
        Assert.assertNotEquals(USER2_TASK_LIST.size(), taskService.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()).size());

        thrown.expect(UserIdEmptyException.class);
        taskService.findAllByProjectId(null, USER2_PROJECT1.getId());
    }

}
